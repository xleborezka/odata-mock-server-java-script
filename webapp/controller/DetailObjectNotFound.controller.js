sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("test.mock.server.masterdetail.controller.DetailObjectNotFound", {});
});