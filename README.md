# Mock Server OData v2

**To read**: []

**Estimated reading time**: 20 minutes

## Story Outline
This story about a mock server and its configuration to start your app with mock data.

## Story Organization
**Story Branch**: master
> 'git checkout master'

Tags: #odata, #mock_server